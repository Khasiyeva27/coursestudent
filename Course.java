public class Course {
    private final String name;
    private final int credit;
    private final CourseStatus status;
    private final String[] keywords;

    public Course(String name, int credit, CourseStatus status, String[] keywords) {
        this.name = name;
        this.credit = credit;
        this.status = status;
        this.keywords = keywords.clone();
    }
    public String getName() {
        return name;
    }

    public int getCredit() {
        return credit;
    }

    public CourseStatus getStatus() {
        return status;
    }

    public String[] getKeywords() {
        return keywords.clone();
    }
}
