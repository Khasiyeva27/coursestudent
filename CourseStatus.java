public final class CourseStatus {
    public static final CourseStatus active = new CourseStatus("Active");
    public static final CourseStatus deactive = new CourseStatus("Deactive");

    public final String name;
    private CourseStatus(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
    public static String findByName(String name) {
        if (name != null) {
            switch (name.toLowerCase()) {
                case "active":
                    return active.getName();
                case "deactive":
                    return deactive.getName();
                default:
                    return "Error: Invalid CourseStatus name";
            }
        } else {
            return "Error: CourseStatus name cannot be null";
        }
    }
}
