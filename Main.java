public class Main {
    public static void main(String[] args) {

        Course course1 = new Course("Mathematics", 3, CourseStatus.active, new String[]{"algebra", "calculus"});
        Course course2 = new Course("English Literature", 4, CourseStatus.deactive, new String[]{"poetry", "prose"});

        System.out.println("Course 1 Status: " + course1.getStatus().getName());
        System.out.println("Course 2 Status: " + course2.getStatus().getName());

        String statusName1 = "salam";
        String statusName2 = "Active";
        String status1 = CourseStatus.findByName(statusName1);
        String status2 = CourseStatus.findByName(statusName2);
        System.out.println("Status Found by Name '" + statusName1 + "': " + status1);
        System.out.println("Status Found by Name '" + statusName2 + "': " + status2);
    }
}
